from pyparsing import Word, alphas, alphanums, nums, Optional, ZeroOrMore, Or, Literal, Combine, ParserElement, Suppress, CaselessLiteral, QuotedString

class Grammar:
    def __init__(self):
        """
        Construct grammar tokens
        See IMAP specification for details (RFC 3501, Formal Syntax section)
        """

        # Treat whitespace characters as significant characters
        # E.g. Word(alphas) + Literal(' ') + Words(alphas)
        # will match 'Hello World' but won't match
        # 'Hello    World'
        ParserElement.setDefaultWhitespaceChars('')

        # Various constants used throughout IMAP specification
        self.FLAGS = CaselessLiteral('flags')
        self.SILENT = CaselessLiteral('.silent')
        self.STORE = CaselessLiteral('store')
        self.FETCH = CaselessLiteral('fetch')
        self.UID = CaselessLiteral('uid')
        self.SP = Suppress(' ')
        self.LP = Suppress('(') # (L)eft (P)arenthesis
        self.RP = Suppress(')') # (R)eft (P)arenthesis
        self.DQ = Literal('"')
        self.OK = CaselessLiteral('ok')
        self.NO = CaselessLiteral('no')
        self.BAD = CaselessLiteral('bad')
        self.BYE = CaselessLiteral('bye')
        self.CRLF = Literal('\r\n')
        self.AUTHENTICATE = CaselessLiteral('authenticate')
        self.NIL = CaselessLiteral('nil')
        self.EXPUNGE = CaselessLiteral('expunge')
        self.BODY = CaselessLiteral('body')

        # Constants for various types of characters used throughout IMAP spec
        self.CTL = ''.join(chr(ch) for ch in range(0x21) + range(0x80, 0x100))
        # All CHARs
        self.Chars = ''.join(chr(ch) for ch in range(0x100))
        # CHAR8 --- x01-ff
        self.Char8 = ''.join(chr(ch) for ch in range(0x01, 0x100))
        # atom-specials in the grammar
        self.nonAtomChars = r'(){%*"\] ' + self.CTL
        # All the bytes that match the ATOM-CHAR from the grammar in the RFC. ("Any CHAR except atom-specials")
        self.atomChars = ''.join(char for char in self.Chars if char not in self.nonAtomChars)
        self.respSpecials = ']'
        self.astringChars = self.atomChars + self.respSpecials
        self.textChars = ''.join(char for char in self.Chars if char not in '\r\n') # Any CHAR except CR and LF
        self.base64Chars = alphanums + '+/'

        # sequence-set
        self.sequence_set = Word('1234567890,:*')('sequence_set')

        self.flag = Or([Literal(r'\Answered') , Literal(r'\Flagged') , Literal(r'\Deleted') , Literal(r'\Seen') , Literal(r'\Draft') , Word(alphas) , Combine(Literal('\\') + Word(alphas))])

        self.flag_list = self.LP +  Optional(self.flag + ZeroOrMore(self.SP + self.flag))('flag_list') + self.RP

        self.store_att_flags = Combine(Optional(Literal('+') ^ Literal('-')) + self.FLAGS +  Optional(self.SILENT))('item_name') + self.SP + (self.flag_list ^ (self.flag + ZeroOrMore(self.SP + self.flag))('flag_list'))

        self.store_modifier = Word(alphanums + ':-_') + Optional(' ' + Word('1234567890,:*'))
        self.store_modifiers = Suppress(' (' + self.store_modifier + ZeroOrMore(' ' + self.store_modifier) + ')')

        self.store = (self.STORE)('STORE') + self.SP + self.sequence_set + Suppress(Optional(self.store_modifiers)) + self.SP + self.store_att_flags

        self.literal = Literal('{') + Word(nums) + Literal('}') + self.CRLF # + *CHAR8 -- Not including this.
        self.quoted = self.DQ + Word(self.Chars, excludeChars=r'\"')('quoted') + self.DQ

        # Either quoted string or a literal
        #self.string = (self.literal)('literal') ^ (QuotedString(quoteChar='"', escQuote=r'\"'))('quoted')
        self.string = (self.literal)('literal') ^ (QuotedString(quoteChar='"', escChar='\\'))('quoted')

        self.header_fld_name = Word(self.astringChars) ^ self.string
        self.header_list = self.LP + self.header_fld_name + ZeroOrMore(self.SP + self.header_fld_name) + self.RP

        self.section_msgtext = Literal('HEADER') ^ Literal('HEADER.FIELDS') + Optional(Literal('.NOT')) + self.SP + self.header_list ^ Literal('TEXT')
        self.section_text = self.section_msgtext ^ Literal('MIME')
        self.section_part = Word(nums) + ZeroOrMore('.' + Word(nums))
        self.section = Literal('[') + Optional(self.section_msgtext ^ (self.section_part + Optional(Literal('.') + self.section_text))) + Literal(']')

        self.bindex = Suppress(self.section + Optional(Literal('<') + Word('1234567890.') + Literal('>')))
        self.fetch_att = Or([Literal('ENVELOPE'), Literal('FLAGS'), Literal('INTERNALDATE'), Combine(Literal('RFC822') + Optional(Or([Literal('.HEADER'), Literal('.SIZE'), Literal('.TEXT')]))), Combine(Literal('BODY') + Optional(Literal('STRUCTURE'))), Literal('UID'), Literal('BODY') + self.bindex, Literal('BODY.PEEK') + self.bindex])

        # Use store_modifiers where fetch_modifiers should be --- they are the same
        self.fetch = (self.FETCH)('FETCH') + self.SP + (self.sequence_set)('sequence_set') + self.SP + Or([Literal('ALL')('item_name'), Literal('FULL')('item_name'), Literal('FAST')('item_name'), self.fetch_att('item_name'), (self.LP + self.fetch_att + ZeroOrMore(self.SP + self.fetch_att) + self.RP)('item_names')]) + Suppress(Optional(self.store_modifiers))

        # UID command may also take COPY and SEARCH, but we don't consider these commands
        self.uid = self.UID('UID') + self.SP + Combine(self.fetch ^ self.store, joinString=' ')('subcommand')

        self.uid_store_fetch = self.store ^ self.fetch ^ self.uid

        self.nstring = (self.NIL)('nil') ^ (self.string)

        self.my_msg_att = (Suppress(self.UID + self.SP) + (Word(nums))('uid')) ^ (Suppress(self.BODY + Literal('[]') + self.SP) + self.nstring)
        self.body_msg_att = self.LP + self.my_msg_att + ZeroOrMore(self.SP + self.my_msg_att) + self.RP

        # w/o CRLF at the end!
        self.my_fetch_response = Suppress('*') + self.SP + Word('123456789', '1234567890')('message_number') + self.SP + self.FETCH + self.SP + self.body_msg_att

        self.authenticate = Suppress(Word(self.astringChars, excludeChars='+')) + self.SP + self.AUTHENTICATE + self.SP + Word(self.atomChars)('mechanism')
        self.authenticate_full = self.authenticate + self.CRLF + Word(self.base64Chars)('credentials')

        self.seq_number = Word('123456789', '1234567890') ^ Literal('*')
        self.seq_range = (self.seq_number)('left') + Suppress(Literal(':')) + (self.seq_number)('right')
        self.seq_part = Combine(self.seq_number ^ self.seq_range)('part')
        self.proper_sequence_set = (self.seq_part + ZeroOrMore(Suppress(Literal(',')) + self.seq_part))('part_list')
