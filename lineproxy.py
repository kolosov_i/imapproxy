from twisted.protocols.portforward import Proxy, ProxyClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet import ssl
from OpenSSL import SSL


class LineProxy(LineReceiver, Proxy):
    """
    A mixin that is here to help build a line receiving
    server that is also a proxy.
    Method resolution order is from left to right, so
    LineReceiver's dataReceived will be called.
    Thus LineProxy will be able to receive data in lines.
    """
    pass

class LineProxyClient(LineProxy):
    """
    See twisted.protocols.portforward.ProxyClient

    A proxy client that receives a line of data
    and uses its peer's transport to pass the line
    further
    """

    def connectionMade(self):
        self.peer.setPeer(self)

        # Wire this and the peer transport together to enable
        # flow control (this stops connections from filling
        # this proxy memory when one side produces data at a
        # higher rate than the other can consume).
        self.transport.registerProducer(self.peer.transport, True)
        self.peer.transport.registerProducer(self.transport, True)

        # We're connected, everybody can read to their hearts content.
        self.peer.transport.resumeProducing()

    def lineReceived(self, line):
        print "S: %s" %line

        # pass data to client
        self.peer.transport.write(line + self.delimiter)


class LineProxyClientFactory(ProxyClientFactory):
    protocol = LineProxyClient


class LineProxyServer(LineProxy):
    """
    See twisted.protocols.portforward.ProxyClient
    """

    def __init__(self):
        self.clientProtocolFactory = LineProxyClient

    reactor = None

    def connectionMade(self):
        # Don't read anything from the connecting client until we have
        # somewhere to send it to.
        self.transport.pauseProducing()

        client = self.clientProtocolFactory()
        client.setServer(self)

        if self.reactor is None:
            from twisted.internet import reactor
            self.reactor = reactor

        self.reactor.connectTCP(self.factory.host, self.factory.port, client)

    def lineReceived(self, line):
        print "C: %s" %line

        # pass data to client
        self.peer.transport.write(line + self.delimiter)


class ClientTLSContext(ssl.ClientContextFactory):
        isClient = 1
        def getContext(self):
            return SSL.Context(SSL.TLSv1_METHOD)


class SSLProxyClient(LineProxyClient):

    def __init__(self):
        self.doStartTLS = False

    def setDoStartTLS(self, startTLS):
        """
        :param startTLS: boolean value indicating if this object
        should attempt to start a TLS session
        """
        self.doStartTLS = startTLS

class SSLProxyClientFactory(ProxyClientFactory):
    protocol = SSLProxyClient

    def __init__(self):
        self.doStartTLS = False

    def buildProtocol(self, *args, **kw):
        prot = ProxyClientFactory.buildProtocol(self, *args, **kw)
        prot.doStartTLS = self.doStartTLS
        return prot

class SSLProxyServer(LineProxyServer):

    def __init__(self):
        self.clientProtocolFactory = SSLProxyClientFactory
        self.doStartTLS = False

    def connectionMade(self):
        # Don't read anything from the connecting client until we have
        # somewhere to send it to.
        self.transport.pauseProducing()

        client = self.clientProtocolFactory()
        client.setServer(self)

        if self.factory.port == 143:
            client.doStartTLS = True

        if self.reactor is None:
            from twisted.internet import reactor
            self.reactor = reactor

        if self.factory.port == 993:
            self.reactor.connectSSL(self.factory.host, self.factory.port, client, ssl.ClientContextFactory())
        elif self.factory.port == 143: # remote port == 143
            self.reactor.connectTCP(self.factory.host, self.factory.port, client)
        else:
            raise Warning("Remote port {} is not supported".format(self.factory.port))


class ServerTLSContext(ssl.DefaultOpenSSLContextFactory):
    def __init__(self, *args, **kw):
        kw['sslmethod'] = SSL.TLSv1_METHOD
        ssl.DefaultOpenSSLContextFactory.__init__(self, *args, **kw)
