import unittest

from imapproxyserver import IMAPProxyServer
from parse import Grammar

class Test(unittest.TestCase):

    def setUp(self):
        self.server = IMAPProxyServer()

        self.g = Grammar()

        self.storeNone = [
            # No \Seen flag, no +flags and no flags
            'STORE 2:4 -flags (\Deleted)',
            'STORE 2:4 -flags (\Deleted)',
            'STORE 2:4 -flags.silent (\Deleted)',
            'STORE 2:4 -flags.silent (\Deleted)',
            'STORE 2:4 -flags (\Deleted)',
            'STORE 2:4 -flags (\Deleted)',
            'STORE 2:4 -flags.silent (\Deleted)',
            'STORE 2:4 -flags.silent (\Deleted)',

            # +flags or flags but no \Seen
            'STORE 2:4 +flags (\Deleted)',
            'STORE 2:4 +flags.silent (\Deleted)',
            'STORE 2:4 -flags (\Deleted)',
            'STORE 2:4 -flags.silent (\Deleted)',
            'STORE 2:4 flags (\Deleted)',
            'STORE 2:4 flags.silent (\Deleted)',
            'STORE 2:4 +FLAGS (\Deleted)',
            'STORE 2:4 +FLAGS.SILENT (\Deleted)',
            'STORE 2:4 -FLAGS (\Deleted)',
            'STORE 2:4 -FLAGS.SILENT (\Deleted)',
            'STORE 2:4 FLAGS (\Deleted)',
            'STORE 2:4 FLAGS.SILENT (\Deleted)',

            # Have \Seen, but no +flags or flags
            'STORE 2:4 -flags (\Deleted \Seen)',
            'STORE 2:4 -flags (\Seen \Deleted)',
            'STORE 2:4 -flags.silent (\Deleted \Seen)',
            'STORE 2:4 -flags.silent (\Seen \Deleted)',
            'STORE 2:4 -flags (\Deleted \Seen)',
            'STORE 2:4 -flags (\Seen \Deleted)',
            'STORE 2:4 -flags.silent (\Deleted \Seen)',
            'STORE 2:4 -flags.silent (\Seen \Deleted)'

            # No \Seen flag, no +flags and no flags; store modifiers
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted)',

            # +flags or flags but no \Seen; store modifiers
            'STORE 2:4 (MOD1 MOD2) +flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) +flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags.silent (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) +FLAGS (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) +FLAGS.SILENT (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -FLAGS (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) -FLAGS.SILENT (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) FLAGS (\Deleted)',
            'STORE 2:4 (MOD1 MOD2) FLAGS.SILENT (\Deleted)',

            # Have \Seen, but no +flags or flags; store modifiers
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) -flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) -flags.silent (\Seen \Deleted)'
        ]

        self.storeSeen = [
            # No store modifiers
            'STORE 2:4 +flags (\Deleted \Seen)',
            'STORE 2:4 +flags (\Seen \Deleted)',
            'STORE 2:4 +flags.silent (\Deleted \Seen)',
            'STORE 2:4 +flags.silent (\Seen \Deleted)',
            'STORE 2:4 +flags (\Deleted \Seen)',
            'STORE 2:4 +flags (\Seen \Deleted)',
            'STORE 2:4 +flags.silent (\Deleted \Seen)',
            'STORE 2:4 +flags.silent (\Seen \Deleted)',
            'STORE 2:4 flags (\Deleted \Seen)',
            'STORE 2:4 flags (\Seen \Deleted)',
            'STORE 2:4 flags.silent (\Deleted \Seen)',
            'STORE 2:4 flags.silent (\Seen \Deleted)',
            'STORE 2:4 flags (\Deleted \Seen)',
            'STORE 2:4 flags (\Seen \Deleted)',
            'STORE 2:4 flags.silent (\Deleted \Seen)',
            'STORE 2:4 flags.silent (\Seen \Deleted)'

            # With store modifiers
            'STORE 2:4 (MOD1 MOD2) +flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) +flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) +flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) +flags.silent (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) +flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) +flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) +flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) +flags.silent (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) flags.silent (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) flags (\Seen \Deleted)',
            'STORE 2:4 (MOD1 MOD2) flags.silent (\Deleted \Seen)',
            'STORE 2:4 (MOD1 MOD2) flags.silent (\Seen \Deleted)'
        ]

        self.fetchNone = [
            # No \Seen being set, no fetch modifiers
            'FETCH 2:4 RFC822.SIZE',
            'FETCH 2:4 (RFC822.SIZE FLAGS)',
            'FETCH 2:4 RFC822.SIZE (MOD1, MOD2)',
            'FETCH 2:4 (RFC822.SIZE FLAGS) (MOD1, MOD2)',
            'FETCH 2:4 (FLAGS BODY.PEEK[HEADER.FIELDS (DATE FROM)])',
            'FETCH 2:4 (FLAGS BODYSTRUCTURE)',
            'FETCH 2:4 (ENVELOPE)',
            'FETCH 2:4 ENVELOPE',
            'FETCH 2:4 RFC822.HEADER', # == BODY.PEEK[HEADER]
            'FETCH 2:4 INTERNALDATE',
            'FETCH 2:4 FLAGS',
            'FETCH 2:4 (FLAGS BODY.PEEK[])',
            'fetch 16 (UID RFC822.SIZE FLAGS BODY.PEEK[HEADER.FIELDS (From To Cc Bcc Subject Date Message-ID Priority X-Priority References Newsgroups In-Reply-To Content-Type)])',
            'fetch 16 (UID RFC822.SIZE BODY.PEEK[])',

            # No \Seen being set, fetch modifiers
            'FETCH 2:4 (FLAGS BODY.PEEK[HEADER.FIELDS (DATE FROM)]) (MOD1 MOD2)',
            'FETCH 2:4 (FLAGS BODYSTRUCTURE) (MOD1 MOD2)',
            'FETCH 2:4 (ENVELOPE) (MOD1 MOD2)',
            'FETCH 2:4 ENVELOPE (MOD1 MOD2)',
            'FETCH 2:4 RFC822.HEADER (MOD1 MOD2)', # == BODY.PEEK[HEADER]
            'FETCH 2:4 INTERNALDATE (MOD1 MOD2)',
            'FETCH 2:4 FLAGS (MOD1 MOD2)',
            'FETCH 2:4 (FLAGS BODY.PEEK[]) (MOD1 MOD2)'

        ]

        self.fetchSeen = [
            'FETCH 2:4 RFC822.TEXT',
            'FETCH 2:4 RFC822',
            'FETCH 2:4 ALL',
            'FETCH 2:4 FAST',
            'FETCH 2:4 FULL',

            'FETCH 2:4 (RFC822.TEXT FLAGS)',
            'FETCH 2:4 (RFC822 FLAGS)',

            # Fetch modifiers
            'FETCH 2:4 RFC822.TEXT (MOD1, MOD2)',
            'FETCH 2:4 RFC822 (MOD1, MOD2)',
            'FETCH 2:4 ALL (MOD1, MOD2)',
            'FETCH 2:4 FAST (MOD1, MOD2)',
            'FETCH 2:4 FULL (MOD1, MOD2)',

            'FETCH 2:4 (RFC822.TEXT FLAGS) (MOD1, MOD2)',
            'FETCH 2:4 (RFC822 FLAGS) (MOD1, MOD2)',

        ]

        self.sequenceSetStrings = [
            '1',
            '*',
            '*:*',
            '1,*:*',
            '1:3,*:*',
            '1:*,*:*',
            '*:1,*:*',
            '1:4,*:*,6',
            '1:4,*:*,6:8',
            '1:4,6:8'
        ]

        self.sequenceSets = [
            set([1]),
            set(),
            set(),
            set([1]),
            set([1,2,3]),
            set([1]),
            set([1]),
            set([1,2,3,4,6]),
            set([1,2,3,4,6,7,8]),
            set([1,2,3,4,6,7,8]),
        ]

        self.fetchResponses = [
            # FETCH UID
            '* 1 FETCH (UID 1)\r\n',
            # FETCH BODY[] literal
            '* 1 FETCH (BODY[] {10}\r\n)\r\n',
            # FETCH BODY[] quoted
            '* 1 FETCH (BODY[] "test")\r\n',
            # FETCH UID BODY[] literal
            '* 1 FETCH (UID 1 BODY[] {10}\r\n)\r\n',
            # FETCH UID BODY[] quoted
            '* 1 FETCH (UID 1 BODY[] "test")\r\n',
            # FETCH BODY[] literal UID
            '* 1 FETCH (BODY[] {10}\r\n UID 1)\r\n',
            # FETCH BODY[] quoted UID
            '* 1 FETCH (BODY[] "test" UID 1)\r\n',

            # Something fancy inside quoted string
            '* 1 FETCH (BODY[] "\\" test\\"")\r\n'
        ]

        self.notFetchResponses = [
            '* 1 FETCH (RFC822.SIZE 11 UID 1)\r\n',
            '* 1 FETCH (RFC822.SIZE 11 BODY[] {10}\r\n)\r\n',
            '* 1 FETCH (RFC822.SIZE 11 BODY[] "test")\r\n',
            '* 1 FETCH (RFC822.SIZE 11 UID 1 BODY[] {10}\r\n)\r\n',
            '* 1 FETCH (RFC822.SIZE 11 UID 1 BODY[] "test")\r\n',
            '* 1 FETCH (RFC822.SIZE 11 BODY[] {10}\r\n UID 1)\r\n',
            '* 1 FETCH (RFC822.SIZE 11 BODY[] "test" UID 1)\r\n',

            '* 1 FETCH (UID 1 RFC822.SIZE)\r\n',
            '* 1 FETCH (BODY[] {10}\r\n RFC822.SIZE)\r\n',
            '* 1 FETCH (BODY[] "test" RFC822.SIZE)\r\n',
            '* 1 FETCH (UID 1 BODY[] {10}\r\n RFC822.SIZE)\r\n',
            '* 1 FETCH (UID 1 BODY[] "test" RFC822.SIZE)\r\n',
            '* 1 FETCH (BODY[] {10}\r\n UID 1 RFC822.SIZE)\r\n',
            '* 1 FETCH (BODY[] "test" UID 1 RFC822.SIZE)\r\n',

            '1 FETCH (BODY[] {10}\r\n)\r\n',
            '1 FETCH (BODY[] "test")\r\n',
            'FETCH FETCH (UID 1)\r\n',
            '1 1 FETCH (BODY[] {10}\r\n)\r\n',
            '1 FETCH (BODY[] "test")',
            '1 FETCH (UID 1 BODY[] {10}\r\n)',
            '1 FETCH (UID 1 BODY[] "test"'
        ]

    def testsequenceSet(self):

        for input in self.sequenceSetStrings:
            raised = False
            try:
                self.g.proper_sequence_set.parseString(input)
            except:
                raised = True

            self.assertFalse(raised, input)

    def teststringToSet(self):

        for index, setString in enumerate(self.sequenceSetStrings):
            self.assertEqual(self.server.stringToSet(setString), self.sequenceSets[index], msg=setString)

    def testmakeStore(self):

        for input in self.storeNone:
            tree = self.g.store.parseString(input)
            self.assertEqual(self.server.makeStore(tree), None, msg=input)

        for input in self.storeSeen:
            tree = self.g.store.parseString(input)
            self.assertEqual(self.server.makeStore(tree), self.server.fetchFmt.format("2:4"), msg=input)

    def testmakeFetch(self):

        for input in self.fetchNone:
            tree = self.g.fetch.parseString(input)
            self.assertEqual(self.server.makeFetch(tree), None, msg=input)

        for input in self.fetchSeen:
            tree = self.g.fetch.parseString(input)
            self.assertEqual(self.server.makeFetch(tree), self.server.fetchFmt.format("2:4"), msg=input)

    def testmakeUid(self):

        for input in [' '.join(('UID', i)) for i in (self.fetchNone + self.storeNone)]:
            tree = self.g.uid.parseString(input)
            self.assertEqual(self.server.makeUid(tree), None, msg=input)

        for input in [' '.join(('UID', i)) for i in (self.fetchSeen + self.storeSeen)]:
            tree = self.g.uid.parseString(input)
            self.assertEqual(self.server.makeUid(tree), ' '.join(('UID', self.server.fetchFmt.format("2:4"))), msg=input)

    def testGrammar(self):

        all = self.storeNone + self.storeSeen + self.fetchNone + self.fetchSeen
        all.extend(["UID " + item for item in all])

        for input in all:
            raised = False
            try:
                self.g.uid_store_fetch.parseString(input)
            except:
                raised = True

            self.assertFalse(raised, input)

        for input in self.fetchResponses:
            raised = False
            try:
                self.g.my_fetch_response.parseString(input)
            except:
                raised = True

            self.assertFalse(raised, input)

        for input in self.notFetchResponses:
            raised = False
            try:
                self.g.my_fetch_response.parseString(input)
            except:
                raised = True

            self.assertTrue(raised, input)

if __name__ == "__main__":
    unittest.main(verbosity=2)
