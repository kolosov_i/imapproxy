import lineproxy as p
import imapproxyclient as c
from parse import Grammar
from pyparsing import ParseException

try:
    import cStringIO as StringIO
except:
    import StringIO


class IMAPProxyServer(p.SSLProxyServer):
    """
    Talks to client.
    """

    fetchFmt = 'FETCH {} (BODY[])'
    uidFmt = 'UID {}'
    g = Grammar()
    _memoryFileLimit = 1024 * 1024 * 10

    def __init__(self):
        p.SSLProxyServer.__init__(self)
        self.clientProtocolFactory = c.IMAPProxyClientFactory
        self._tag = 1 # Tag to be sent to backend
        self.doFetch = None
        self.myNumber = 0
        self.originalTag = {} # Tag sent to backend -> original tag
        self._parts = None # Buffers command
        self._currentTag = None
        self._seqNumbers = None
        self._UIDs = None
        self.user = None

    @property
    def tag(self):
        """
        Returns the tag to be sent to backend
        and increments internal tag value.
        """
        currentTag = str(self._tag)
        self._tag = self._tag + 1
        return currentTag

    def sendCommandToBackend(self, command, originalTag=None):
        """
        :param command: IMAP command, i.e. part after the tag
        :param originalTag: original tag sent by client. If it's
         set to None, then this command has never been sent by client

        Prefix command with current tag and send it to backend.
        If this command was issued by client, remember its original
        tag so we can send tagged response to client later.
        """

        tag = self.tag # New tag

        # tag -> originalTag
        if originalTag is not None:
            self.originalTag[tag] = originalTag

        newcommand = ' '.join((tag, command)) + self.delimiter

        print "C: {}".format(newcommand.rstrip('\r\n'))
        self.peer.transport.write(newcommand) # send to backend

    def makeLine(self, command):
        """Make a command (CRLF terminated line)
           :param command: the part of command following the tag
        """
        return ' '.join((self.tag, command)) + self.delimiter

    def toInt(self, s):
        """
        :param s: str
        Return an integer that the string s represents
        or None if s does not represent an integer
        """
        try:
            return int(s)
        except ValueError:
            return None

    def stringToSet(self, sequenceSetString):
        """
        :param sequenceSetString: str

        Return a set of numbers contained in the sequence set that
        sequenceSetString represents. May return an empty set.
        Doesn't account for asterisks.
        """
        try:
            tree = self.g.proper_sequence_set.parseString(sequenceSetString)
        except ParseException:
            return

        sset = set()

        for item in tree['part_list']:
            if 'left' in item and 'right' in item:
                left = self.toInt(item['left'])
                right = self.toInt(item['right'])
                if left is not None and right is not None:
                    sset.update(range(left, right + 1))
                elif left is not None:
                    sset.add(left)
                elif right is not None:
                    sset.add(right)
            else: # single number
                try:
                    sset.add(int(item))
                except ValueError:
                    pass

        return sset

    def addSequenceSet(self, sequenceSetString, UID):
        """
        :param sequenceSetString: str
        :param UID: True if the numbers in sequence set are UIDs
        """
        sset = self.stringToSet(sequenceSetString)

        if not UID:
            if self._seqNumbers is None:
                self._seqNumbers = sset
            else:
                self._seqNumbers.update(sset)
        else:
            if self._UIDs is None:
                self._UIDs = sset
            else:
                self._UIDs.update(sset)

    def makeStore(self, tree):
        """
        :param tree: a pyparsing.ParseResults instance

        Return None if STORE command with argument 'args'
        does not set the \Seen flag, otherwise return a
        fetch command that fetches the bodies of messages
        that are affected by STORE.
        """

        if tree['item_name'].lower() in ('flags', 'flags.silent', '+flags', '+flags.silent'):
            for flag in tree['flag_list']:
                if flag == r'\Seen':
                    return self.fetchFmt.format(tree['sequence_set'])

        return None

    def makeFetch(self, tree):
        """
        :param tree: a pyparsing.ParseResults instance

        Return None if tree does not represent a FETCH command that
        affects the \Seen flag, otherwise return a FETCH command that
        fetches bodies of the messages specified in sequence set
        """

        if 'item_name' in tree:
            items = [tree['item_name'].lower()]
        else:
            items = [item.lower() for item in tree['item_names']]

        for item in items:
            if item in ('rfc822', 'rfc822.text', 'body', 'all', 'full', 'fast'):
                return self.fetchFmt.format(tree['sequence_set'])

        return None

    def makeUid(self, tree):
        """
        :param tree: a pyparsing.ParseResults instance

        Return None if tree does not represent a UID command that
        affects the \Seen flag, otherwise return a FETCH command that
        fetches bodies of the messages specified in sequence set
        """

        subtree = tree['subcommand']

        if 'STORE' in subtree:
            add = self.makeStore(subtree)
        elif 'FETCH' in subtree:
            add = self.makeFetch(subtree)
        else:
            add =  None

        if add is not None:
            return ' '.join(('UID', add))
        return None

    def sendCommand(self, line):
        """
        :param line: a line received from client

        Looks if line represents an IMAP command and tries to see
        if this command will set the \Seen flag. In this case,
        the server sends an additional FETCH command to backend
        in order to fetch bodies of the messages that were \Seen.

        If a STARTTLS command was issued, server begins STARTTLS
        negotiation.
        """

        try:
            tag, command = line.split(' ', 1)
        except ValueError: # cannot be splitted
            self.peer.transport.write(''.join((line, self.delimiter)))
            return

        if command.lower() == 'starttls':
            self.transport.write(' '.join((tag, 'OK Starting TLS.\r\n')))

            ctx = p.ServerTLSContext(
            privateKeyFileName='keys/key.pem',
            certificateFileName='keys/cert.pem',
            )

            self.transport.startTLS(ctx, self.factory)
            return # don't send STARTTLS to backend

        else:
            additional = None

            try:
                tree = self.g.uid_store_fetch.parseString(command)
            except ParseException: # Not the command we are looking for
                self.sendCommandToBackend(command, tag)
                return

            if 'UID' in tree:
                additional = self.makeUid(tree)
            elif 'STORE' in tree:
                additional = self.makeStore(tree)
            elif 'FETCH' in tree:
                additional = self.makeFetch(tree)

            if additional is not None:
                self.addSequenceSet(tree['sequence_set'] if 'sequence_set' in tree else tree['subcommand']['sequence_set'], 'UID' in tree)
                self.sendCommandToBackend(additional)

            self.sendCommandToBackend(command, tag)

    def processCommand(self, parts):
        """
        :param parts: list of command parts

        Try to see if this is an AUTHENTICATE PLAIN command
        """

        if len(parts) == 1:
            self.sendCommand(parts[0])
        elif self.authPlain: # This last part is a base64-encoded string
            self.authPlain = False
            try:
                dec = parts[-1].decode('base64')
                # NUL<login>NUL<password>
                self.user = dec[1:dec.find(chr(0), 1)]
            except:
                pass
            # Send the last part, others have been sent before
            self.peer.transport.write(''.join((parts[-1], self.delimiter)))

    def _setupForAuth(self, line):
        """
        :param line: an authentication command

        Send previous command to make place for this one
        """
        newTag = self.tag
        self.originalTag[newTag], rest = line.split(' ', 1)

        fullPart = ' '.join((newTag, rest)) + self.delimiter
        self._parts = [fullPart]
        print 'C{}: {}'.format(self.myNumber, fullPart.rstrip('\r\n')),

        self.peer.transport.write(fullPart)

    def _setupForLiteral(self, rest, octets):
        """
        :param rest: part of line before literal
        :param octets: number of octets to come
        """
        self._literalBuffer = self.messageFile(octets)
        self._literalSize = octets

        if self._parts is None: # A new command
            try: # new command - try to substitute tag
                newTag = self.tag
                self.originalTag[newTag], afterTag = rest.split(' ', 1)
                fullPart = ' '.join((newTag, afterTag)) + self.delimiter
            except ValueError:
                fullPart = ''.join((rest, self.delimiter))

            self._parts = [fullPart]
        else:
            fullPart = ''.join((rest, self.delimiter))
            self._parts.extend([fullPart])

        print 'C{}: {}'.format(self.myNumber, fullPart.rstrip('\r\n')),

        self.peer.transport.write(fullPart)

        self.setRawMode()

    def messageFile(self, octets):
        """
        :param octets: number of bytes in literal
        Taken from twisted.mail.imap4 class IMAP4Client

        Return a buffer for literal.
        Return a StringIO buffer if literal size is not too big,
        otherwise create a temporary file and return it.
        """
        if octets > self._memoryFileLimit:
            return tempfile.TemporaryFile()
        else: # literal's not too big, buffer in memory
            return StringIO.StringIO()

    def rawDataReceived(self, data):
        """
        :param data: bytes (i.e. str)

        Receive bytes as long as there are more to come,
        then move over to line mode
        """

        self._literalSize -= len(data)

        if self._literalSize > 0:
            self._literalBuffer.write(data)
            self.peer.transport.write(data) # send to backend
        else:
            passon = ''
            if self._literalSize < 0:
                data, passon = data[:self._literalSize], data[self._literalSize:]
            self._literalBuffer.write(data)
            self.peer.transport.write(data) # send to backend
            rest = self._literalBuffer
            self._literalBuffer = None
            self._literalSize = None

            # rewind buffer and append its contents
            rest.seek(0, 0)
            self._parts.append(rest.read())
            print 'C{}: {}'.format(self.myNumber, self._parts[-1])
            # set line mode and parse argument for lines
            self.setLineMode(passon)

    def lineReceived(self, line):
        """
        :param line: line (str)

        See if this line starts a new expression or
        continues the current one.
        """

        lastPart = line.rfind('{')
        if lastPart != -1:
            lastPart = line[lastPart + 1:]
            if lastPart.endswith('}'):
                # It's a literal a-comin' in
                try:
                    # How many octets are there?
                    octets = int(lastPart[:-1])
                except ValueError:
                    raise IllegalServerResponse(line)

                self._setupForLiteral(line, octets)
                return

        try:
            authenticate = self.g.authenticate.parseString(line)
            if authenticate['mechanism'].lower() == 'plain':
                self.authPlain = True
        except ParseException:
            authenticate = None

        # If this is a whole expression, i.e.
        # _parts is None and it's not AUTHENTICATE
        if self._parts is None and authenticate is None:
            # It isn't a literal at all
            self.processCommand((line,)) # 1-element tuple..
        elif self._parts is None and authenticate is not None: # Start a new expression
            self._setupForAuth(line)
            return
        else: # An expression has finished
            self._parts.append(line)
            parts = self._parts
            self._parts = None
            self.processCommand(parts)
