import lineproxy as p
from twisted.mail.imap4 import IllegalServerResponse
import tempfile
from pyparsing import ParseException
from parse import Grammar
import datetime
import os
import sqlite3

try:
    import cStringIO as StringIO
except:
    import StringIO

# Module-wide sqlite connection
conn = None

def setupTable():
    """
    Create a table with name 'messages' and fields
    date, user and messageBody of type TEXT
    """

    global conn
    c = conn.cursor()

    # message body is stored as a blob,
    # because it arrives as a sequence of bytes
    # date - UTC time in ISO format
    c.execute(
            '''CREATE TABLE messages
            (date TEXT,
            user TEXT,
            messageBody BLOB)'''
            )

    conn.commit()

def setupDB(dbPath):
    """
    @param dbPath: path to DB file
    """

    global conn

    newDb = not os.path.isfile(dbPath)
    conn = sqlite3.connect(dbPath)

    if newDb:
        setupTable()
    else:
        c = conn.cursor()

        # throws sqlite3.DatabaseError if it's not a database
        c.execute('pragma schema_version;').fetchone()

        mytable = c.execute('select name from sqlite_master where type = "table" and name = "messages"').fetchone()
        if mytable is None:
            setupTable()

class IMAPProxyClient(p.SSLProxyClient):
    """
    Listens to backend.
    lineReceived, rawDataReceived, messageFile and _setupForLiteral
    are taken from twisted.mail.imap4.IMAP4Client almost verbatim.
    """

    _memoryFileLimit = 1024 * 1024 * 10
    g = Grammar()

    # set by ConfProxyFactory
    dbPath = None

    def __init__(self):
        p.SSLProxyClient.__init__(self)

        self.startTLSTag = None
        self.doFetch = None

        # buffers a part of individual response,
        # i.e. continue-req or response-data.
        # This is a list.
        self._parts = None
        self._literalBonds = None
        self._numOfLiterals = 0


    def sendTaggedResponseToClient(self, rest, tag):
        """
        If tag has no corresponding original tag,
        don't send this response. Otherwise send it
        with original tag.
        @param rest: part of response after the tag
        @param tag: tag
        """
        try:
            originalTag = self.peer.originalTag[tag]
            del self.peer.originalTag[tag]
            newcommand = ' '.join((originalTag, rest)) + self.delimiter
            print 'S{}: {}'.format(self.peer.myNumber, newcommand.rstrip('\r\n'))
            self.peer.transport.write(newcommand)
            # send to client with original tag
        except KeyError:
            pass

    def sendCommandToBackend(self, command, originalTag=None):
        """
        :param command: IMAP command, i.e. part after the tag
        :param originalTag: original tag sent by client. If it's
         set to None, then this command has never been sent by client

        Prefix command with current tag and send it to backend.
        If this command was issued by client, remember its original
        tag so we can send tagged response to client later.
        """
        tag = self.peer.tag # take new tag from ProxyServer

        # tag -> originalTag
        if originalTag is not None:
            self.peer.originalTag[tag] = originalTag

        newcommand = ' '.join((tag, command)) + self.delimiter

        self.transport.write(newcommand)
        return tag

    def connectionMade(self):
        p.SSLProxyClient.connectionMade(self)

        if self.doStartTLS == True:
            # remember the tag that is going to be used
            self.startTLSTag = self.sendCommandToBackend('STARTTLS')

    def messageFile(self, octets):
        """
        Taken from twisted.mail.imap4 class IMAP4Client
        """
        if octets > self._memoryFileLimit:
            return tempfile.TemporaryFile()
        else: # literal's not too big, buffer in memory
            return StringIO.StringIO()

    def rawDataReceived(self, data):
        """
        :param data: bytes (i.e. str)

        Receive bytes as long as there are more to come,
        then move over to line mode
        """

        self._literalSize -= len(data)

        if self._literalSize > 0:
            self._literalBuffer.write(data)
        else:
            passon = ''
            if self._literalSize < 0:
                data, passon = data[:self._literalSize], data[self._literalSize:]
            self._literalBuffer.write(data)
            rest = self._literalBuffer
            self._literalBuffer = None
            self._literalSize = None

            # rewind buffer and append its contents
            rest.seek(0, 0)
            self._parts.append(rest.read())
            # set line mode and parse argument for lines
            self.setLineMode(passon.lstrip(self.delimiter))

    def _setupForLiteral(self, rest, octets):
        """
        @param rest: part of line before literal
        """
        self._numOfLiterals += 1

        if self._literalBonds == None:
            left = len(rest) + 2 # line + CRLF
            self._literalBonds = (left, left + octets)

        self._literalBuffer = self.messageFile(octets)
        self._literalSize = octets

        if self._parts is None:
            self._parts = [rest, self.delimiter]
        else:
            self._parts.extend([rest, self.delimiter])

        self.setRawMode()

    def tryMyFetch(self, response, numOfLit, bonds):
        """
        @param response: Full response text

        Return None if this is not my kind of FETCH
        response, otherwise return message body. (A buffer into string)

        Return False if body is 'NIL'
        """
        if numOfLit > 1:
            return None

        # Response w/o literals.
        nolit = response
        if bonds is not None:
            nolit = response[:bonds[0]] + response[bonds[1]:]

        try:
            tree = self.g.my_fetch_response.parseString(nolit)
        except ParseException:
            return None

        if 'uid' in tree:
            number = int(tree['uid'])
            if self.peer._UIDs is None or number not in self.peer._UIDs:
                return None
            self.peer._UIDs.remove(number)
        else:
            number = int(tree['message_number'])
            if self.peer._seqNumbers is None or number not in self.peer._seqNumbers:
                return None
            self.peer._seqNumbers.remove(number)

        # Looks like we have to extract the body...
        if 'quoted' in tree:
            return buffer(tree['quoted'])
        elif 'literal' in tree: # return literal. This check must be obsolete.
            return buffer(response, bonds[0], bonds[1] - bonds[0])
        elif 'nil' in tree:
            return False

        return None

    def saveToDb(self, body):
        """
        @param body: buffer, value of BODY[] message data item

        Save message body, username and current time to db.
        """

        global conn

        c = conn.cursor()

        curDate = datetime.datetime.utcnow().isoformat()

        c.execute(
                'INSERT INTO messages VALUES (?, ?, ?)',
                (curDate, self.peer.user, body)
                )

        conn.commit()

    def processResponse(self, response, numOfLit=0, bonds=None):
        """
        :param response: IMAP response
        :param numOfLit: number of literals in this response
        :param bonds: tuple of boundaries of the first literal

        If response is tagged, try to extract message body
        """
        try:
            tag, rest = response.split(' ', 1)
        except ValueError:
            print 'S{}: {}'.format(self.peer.myNumber, response)
            self.peer.transport.write(response + self.delimiter)
            return

        if tag == '*':
            body = self.tryMyFetch(response, numOfLit, bonds)
            if body is not None:
                if body == False:
                    body = None
                # process the body
                self.saveToDb(body)
                return # don't send to backend.
            print 'S{}: {}'.format(self.peer.myNumber, response)
            self.peer.transport.write(response + self.delimiter)
        elif tag == '+':
            print 'S{}: {}'.format(self.peer.myNumber, response)
            self.peer.transport.write(response + self.delimiter)
        else: # tagged response
            if self.doStartTLS == True and tag == self.startTLSTag:
                if 'ok' in rest.lower(): # may be 'BAD' instead
                    ctx = p.ClientTLSContext()
                    self.transport.startTLS(ctx, self.factory)
                return # don't send this to client

            self.sendTaggedResponseToClient(rest, tag)

    def lineReceived(self, line):
        """
        :param line: line (str)

        Try to detect literal in server response and buffer. Otherwise,
        try to process current expression if it's done.
        """

        lastPart = line.rfind('{')
        if lastPart != -1:
            lastPart = line[lastPart + 1:]
            if lastPart.endswith('}'):
                # It's a literal a-comin' in
                try:
                    # How many octets are there?
                    octets = int(lastPart[:-1])
                except ValueError:
                    raise IllegalServerResponse(line)

                self._setupForLiteral(line, octets)
                return

        if self._parts is None:
            # It isn't a literal at all
            self.processResponse(line)
        else:
            # If an expression is in progress, no tag is required here
            # Since we didn't find a literal indicator, this expression
            # is done.
            self._parts.append(line)
            rest, numOfLit, bonds = ''.join(self._parts), self._numOfLiterals, self._literalBonds

            self._parts, self._numOfLiterals, self._literalBonds = None, 0, None
            self.processResponse(rest, numOfLit, bonds)


class IMAPProxyClientFactory(p.SSLProxyClientFactory):
    protocol = IMAPProxyClient
