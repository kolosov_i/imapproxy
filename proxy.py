from twisted.protocols.portforward import ProxyFactory
from twisted.internet import ssl, reactor

import ConfigParser

import imapproxyserver
import imapproxyclient


class ConfProxyFactory(ProxyFactory):
    conf = None

    def __init__(self, _conf):
        """
        :param _conf: configuration (ConfigParser.ConfigParser instance)

        Set ports and hosts from configuration. Setup database
        """

        self.currentNumber = 1
        self.conf = _conf
        self.protocol = imapproxyserver.IMAPProxyServer

        port = self.conf.getint('local', 'port')

        # call imapproxyclient.setupDB
        imapproxyclient.setupDB(self.conf.get('local', 'db'))

        if port != 993 and port != 143:
            raise Warning("Port {} is not supported".format(port))

        ProxyFactory.__init__(self, _conf.get('remote', 'host'), _conf.getint('remote', 'port'))

    def buildProtocol(self, *args, **kw):
        """
        Overrides buildProtocol in ProxyFactory

        Builds protocol and sets its doStartTLS if local port is set to 143.
        Sets its number
        """

        prot = ProxyFactory.buildProtocol(self, *args, **kw)

        if self.conf.getint('local', 'port') == 143:
            prot.doStartTLS = True

        prot.myNumber = self.currentNumber
        self.currentNumber = self.currentNumber + 1

        return prot


def main():
    """
    Parse config and start listening on specified port
    """

    conf = ConfigParser.ConfigParser()
    conf.read('config.conf') # fix this...

    pfactory = ConfProxyFactory(conf)

    port = conf.getint('local', 'port')

    if port == 993:
        reactor.listenSSL(port, pfactory, ssl.DefaultOpenSSLContextFactory(conf.get('local', 'key'), conf.get('local', 'cert')))
    else:
        reactor.listenTCP(port, pfactory)

    reactor.run()

if __name__ == '__main__':
    main()
